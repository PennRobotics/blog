## Checklist

- [x] create GitLab Action for building and hosting in subfolder
- [x] ensure build throws no errors
- [ ] continue theming
    - [ ] **style.css**
    - [ ] **pennrobotics.py**
    - [ ] font faces (host locally or not at all) -- see below!
    - [ ] which Markdown extensions to add/remove in **build.py**
    - [ ] reduce license footer length (and possibly change overall width of container and footer font size)
- [ ] use latest, stable, preferred versions of dependencies
    - [ ] FA
    - [ ] Bootstrap
    - [ ] Pygments
    - [ ] Markdown extensions
    - [ ] Jinja
    - [ ] etc.
- [ ] ensure complete removal of original author's content
    - [x] no Google Analytics!
    - [x] Main page
    - [ ] Meta-data
        - [x] removed from index
        - [ ] removed from posts
        - [ ] anywhere else?
    - [ ] Social links
        - [x] direct link
        - [x] GitLab
        - [ ] fix: https://www.linkedin.com/in/pennrobotics/
        - [ ] what else needs adding?
    - [ ] RSS feed
        - [x] plain-text viewable on site
        - [ ] from a RSS feed reader
    - [ ] Posts
    - [ ] Templates?
- [ ] are any linked paths not accessible e.g. stylesheets? (there is probably a web-based tool to automate this check)
- [ ] double-check and version-pin **requirements.txt**
- [ ] GDPR checks (such as fonts via CDN)
- [ ] European imprint and data security statements, if needed
- [ ] code coverage: **serve.py**, **__pycache__**, **env**, **.nox**, **docs**, etc.
- [ ] code style
    - [ ] single-quote strings
    - [ ] DRY + fix all relative and absolute URLs (e.g. heavier use of {{root}})
    - [ ] etc.
- [ ] switch GitLab Action to run on commits
    - [ ] test the "draft" regex (which should repress a pipeline when the commit message first line contains "draft")
- [ ] Ensure entire custom domain is accessible
    - [x] Check that both website source trees are accessible: this static site and the source-only pennrobotics.gitlab.io
    - [ ] Fix custom domain access: https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#multi-project-pipelines
- [ ] write content
    - [ ] hello world
- [ ] looks good in responsive?
- [ ] how to handle updates to generator vs new content?
    - [ ] use the **srcs** or **docs** folder to determine if the _deploy_ stage shall run
    - [ ] use tags/releases automatically during pipeline?


### Fonts to try

#### serif
- https://fonts.google.com/specimen/Gloock
- https://fonts.google.com/specimen/Kalnia
- https://fonts.google.com/specimen/Young+Serif

#### sans
- https://fonts.google.com/specimen/AR+One+Sans
- https://fonts.google.com/specimen/Gabarito
- https://fonts.google.com/specimen/Golos+Text
- https://fonts.google.com/specimen/Hedvig+Letters+Sans
- https://fonts.google.com/specimen/Inclusive+Sans
- https://fonts.google.com/specimen/Instrument+Sans
- https://fonts.google.com/specimen/Mooli
- https://fonts.google.com/specimen/Onest
- https://fonts.google.com/specimen/Ysabeau+Infant

#### mono
- https://fonts.google.com/specimen/Sometype+Mono
- https://fonts.google.com/specimen/Victor+Mono

#### odd
- https://fonts.google.com/specimen/Linefont


## GitLab Icon

[The icon](https://gitlab.com/uploads/-/system/project/avatar/54936511/pyblog.png) is a Python-themed [Big Honking Button](https://honk.wntr.dev/).

---
title: Quieter CQ-20B
date: 2024-03-13
description: Reducing the operating noise of a compact digital mixer
---

<mark>check static site compile</mark>  

<mark>TODO: add more links to anything mentioned</mark>

<mark>TODO: add pictures</mark>

<mark>TODO: proofread, check sentence flow in case some paragraphs conveyed their meaning poorly</mark>

## Main problem to fix

The fan on my reasonably expensive mixer is loud enough to hear with a sensitive microphone.

### Solution

Switch out fan with Noctua, and consider replacing the CPU heatsink if the improvement is too modest.

I also reveal the internals of the CQ-20B. <mark>TODO</mark>


## Replacing one great interface with three

I decided to replace my MOTU M4 with a bigger stage mixer, a smaller field recorder, and a tiny inline adapter. The only hitch? The M4 was silent. A 20-channel live audio mixer is not.

### Allen &amp; Heath CQ-20B

Allen &amp; Heath are among the standard-bearers in mixers for live audio. In scraping the Thomann API for deals, I came across a returned and discounted CQ-20B, which is an ASIO-compatible digital audio mixer with 16 microphone inputs, audio I/O between computer/SD card/USB/Bluetooth, and a whole host of effects and automated functions including feedback elimination and mic input leveling. I had just returned my 4-input USB audio interface due to some strange ground loop with a shotgun mic, plus I bought a drum set for the family, and these were the main factors in filling that fresh hole in my sound equipment with the CQ-20B.

<mark>add below as footnote</mark>

(While single-mic and four-mic configurations are nice for a drum kit, I love the sound of the Audix D6 and found a good deal on a set of E604 tom mics. Add a pair of overheads and that&rsquo;s already six channels. Snare? Reinforcement? I can already use half of the inputs on the CQ.)

While the noise floor might be higher on the Allen &amp; Heath than on the audio interface I had been using, I found a nice deal on a Zoom F3, which lets me have portability and a reasonably great preamp.

On the far end of convenience, I have the Shure MVX2U for an individual microphone powered by (and outputting audio to) a USB-C device. It is noisy but super-portable and works with all types of mics.

## Popping the hood on the CQ

I searched online for disassembly photos of the Allen &amp; Heath mixer but found none. Thus, I will take a few photos and contribute what I find.

A few PCB markings near solder points that immediately drew my attention: `LINUX BOOT`, `UART_0`, `LINUX TRACE`, `JTAG`, `DEBUG`, plus there is a small 10-pin header with pin 7 duller than the others, and I would guess that is a bed-of-nails (but not Tag-Connect) MIPI-10 connector. That is half a dozen different potential connections to the inner workings of this CPU. One of the connectors even has pins.

_I might revisit this PCB region in the future&hellip;_

The design is startlingly well-engineered. Voltage regulation at various levels (6.5, 5.0, 3.3 for Wi-Fi, 1.8, 1.5) is clearly marked on the silkscreen. It looks like there is a second fan connector, CN5. Connection subsystems are obvious (USB, ethernet, etc.) and chips are not obfuscated.

When I replace the fan, I might take a better look at the underside of this board as well as the audio board. <mark>TODO</mark>

## Fan

The installed fan is a 60mm [NMB](https://nmbtc.com/), model 06010KA-12L-AU rated at 130 mA. There is only one problem: this fan does not exist _even in the current NMB catalog!_

### Specs

Without a noise meter, I will try to back out the specs of this cooling fan by decoding the model number and seeing which products are most similar. The dimensions are given by the first 5 numbers and the voltage is the number in the second group. What, then, are "KA", "L", and "AU"?

_Spoiler alert: NMB has a [part number decoder](https://nmbtc.com/fan-part-numbering-system/) which I found after meticulously searching their online catalog._

Using the filter on their website, there are models that have the same specifications but the last group is "AT", "AL", and "AA". Opening these, "AT" indicates "Tach Output", "AL" is "Locked Rotor Output", and "AA" is "Standard two-wire".

Next, in comparing two AT motors, it seems the letter next to the voltage indicates input power or a related factor like fan speed. In one case, a 12J fan operates at 40 mA and 1800 RPM while a similar-looking 12K fan spins at 70 mA and 3100 RPM.

On the NMB webpage, the first two letters are decoded: SA and KA are standard ball bearing fans. SS has sleeve bearings. DA are high-performance models for servers. VA and RA are high-performance industrial-grade and KL are legacy models with ball bearings. Unfortunately, KA does not show up often and seemingly never alongside SA.

Still, three 60 &times; 15 mm models have these characteristics:

- "VA" is 14.5 CFM, 38.8 Pa, 4100 RPM, 28.5 dB, 80 mA
- "KA" is 14.1 CFM, 31.3 Pa, 3600 RPM, 28.0 dB, 90 mA
- "SS" is 17.0 CFM, 22.4 Pa, 3200 RPM, 26.0 dB, 80 mA

Then, a similar 60 &times; 20 mm model:

- "SA" is 16.6 CFM, 34.7 Pa, 3600 RPM, 26.0 dB, 60 mA

For 10 mm deep, 60 mm wide fans, there are not so many listings. Notably, nearly every KA-12L fan at a different dimension has between 25 and 29 dB noise level at max speed, but max speed is fairly fast for each model. The fans that are 10 &times; 60 are:

- "2404KL-04W-B5x-Bx0" at 19.1 CFM, 30.0 Pa, 4800 RPM, 36.0 dB, 270 mA
- "2404KL-04W-B4x-Bx0" at 16.2 CFM, 23.0 Pa, 4200 RPM, 33.0 dB, 200 mA
- "2404KL-04W-B3x-Bx0" at 13.8 CFM, 16.7 Pa, 3600 RPM, 29.0 dB, 100 mA

Furthermore, voltage does not affect other specs. That is clear comparing 12 and 24 volt models.

Diving deeper into the catalog, AU indicates a PWM model. All of the listed PWM models are 20 mm and thicker.

Thus, I would venture a guess that this motor is like those from the last list:

- "2404KL-interpolated" at 14.5 CFM, 18.6 Pa, 3800 RPM, 30.2 dB, 130 mA

This pretty closely tracks to the trend between the first-listed KA and SA models. It is possible the CFM is slightly lower, and we have a general idea of the max speed and noise.

### Choosing a replacement

I once observed an internet discussion about fan selection that was quite rabid&mdash;as these tend to be. On one side, someone wanted to replace a fan with a quieter fan and just picked out a fan with better noise specifications. His opponent was quite vocal that details like airflow volume and static pressure are critical specifications and should never be ignored. In this debate, I stand with the former. The fan in this mixer is evidently a 12 volt PWM (4 pins) and there is a temperature sensor that determines the speed of the fan. If the fan is slightly inefficient because it prioritizes the wrong design goal **but still quieter than the incumbent while reaching the target temperature** then I will gladly take the quieter, less efficient option.

Noctua is like the Allen &amp; Heath of tiny windmills. While there might be competitors that best them in noise or longevity or efficiency, they seem to be the pace car for ultra-quiet, long-lasting PC fans.

I do not think a spinny turbine is going to produce _no_ airflow. As a bonus, there are not many options for 60mm, 12 volt, 4 pin fans from Noctua. In fact, there is _one_. (It is taller, but there seems to be enough room in the case for the extra height, and I also think the extra size helps with pushing air.) With that, a twenty euro exchange is bringing a Noctua NF-A6x25 PWM to my door.

To be thorough, the specifications for the NF-A6x25 PWM:

- 17.9 CFM, 21.4 Pa, 3000 RPM, 19.3 dB, 80 mA in _regular_ configuration
- 13.3 CFM, 12.7 Pa, 2300 RPM, 13.7 dB with _low noise adapter_

I think this will be able to push the same amount of air at about 65 percent of the rotational speed and possibly 15 dB quieter, which should be about a quarter of the audible noise level. I would chalk that up mainly to using a hydrodynamic oil bearing vs steel ball, although the extra length would let the NMB catch up; they have a 60 &times; 25 with 13.7 CFM, 20.6 Pa, 2950 RPM, 23 dB, 70 mA; another (06025SA-12K-EL-D0) with slightly better specs: 14.5 CFM, 26.0 Pa, 3000 RPM, 21 dB, 60 mA (although similar models seem to be mistakenly marked at 17 dB with otherwise identical specifications).

### And then?

If changing the fan does not have an appreciable effect, it will probably be time to switch out the CPU heatsink with something mightier and/or more efficient.


## Outcome

Here are the before-and-after measurements:

<mark>TODO</mark>

---
title: Debugging an Electric Fiddle
date: 2024-05-06
description: Reducing the operating noise of an electronic violin
---

<mark>check static site compile</mark>  

<mark>TODO: add more links to anything mentioned</mark>

<mark>TODO: add pictures</mark>

<mark>TODO: proofread, check sentence flow in case some paragraphs conveyed their meaning poorly</mark>

## Problems to fix

The background noise of my practice fiddle is constant and loud, even clearly audible with the volume at its lowest setting. There is also some static while changing the volume. The EQ faders seem to do nothing. It seems only the left channel outputs using the _Line Out_ connector (although this has considerably less noise than the headphone output.) I'm unsure if the auxiliary port functions. Finally&mdash;and this might not be fixable&mdash;low notes tend to overdrive the amplifier.

### Solution

<mark>TODO</mark>


## Disassembly and measurement

I wish I had better documented the disassembly process, but it is fairly straightforward:

1. Loosen the strings until they barely pluck a sound, or just remove them altogether.
2. Remove the bridge
3. Pull out the shoulder rest if it is installed
4. Unscrew the four pairs of screws that look alike. These are the ones where the plastic arches of the body shape section intersect with the fingerboard plus another pair near the battery door hinge. (The four screws holding a plate do not need removal. This plate is only protection for the modular body and fingerboard. The single offset screw between the EQ faders and battery holder is AFAIK only to hold that cable bracket thing.)
5. Remove the chin rest. You can do it the official way by inserting a turning tool and loosening the two mounting bars or just force it off, although the latter method might damage the cork&mdash;as it would for most violins.
6. With controlled pressure, slide the _neck side_ of the plastic body shape away from the fingerboard. Some of the finish might chip off.
7. After getting some distance between the two parts, you'll see the cable connecting the pickup to the electronics PCB. Disconnect this cable. Ideally, use a sharp screwdriver to wiggle between the connector and its socket, but pulling straight out using the cable with gentle wiggling should not damage the connector. Remove the pickup.
8. The tailpiece loop should start coming off the endpin at this point. (I had to look up those terms.) There's a little plastic bracket where the chin rest sits that might fall off. Keep opening the body internals until the loop is completely off.
9. Unscrew the six PCB mount screws.
10. Remove the three knobs from the EQ faders.
11. Optionally, take off the plastic plate that surrounds the _Line Out_ and headphone jack sockets.
12. Desolder the battery connector wires from the PCB.
13. Remove the PCB.

### Operating fundamentals

The electric violin should have the following layout:

- a reasonably standard violin structure: strings, tuners, a bridge, fingerboard, and comfort accessories (chin and should rests)
- a piezoelectric (PZ) pickup that sits between the bridge and body
- a power source
- an amplifier

#### PZ Pickup

This is straightforward: pressure/vibration is converted to an electrical signal by the pickup. This pickup sits under the entire bridge, so it should handle a pressure of up to 100 N. (Most bridges experience between 7 and 9 kg of pressure. This depends on the design geometry and strings.)

Because much of the noise seems unchanging between high and low volume, we can assume at the beginning that the pickup is not a significant factor in reducing noise. At least&hellip; not yet.

#### Amplification Electronics

One positive aspect of the amplifier PCB is that everything is well-marked and layed out in a way that makes sense. You can clearly identify that the battery negative terminal connects to a PCB ground plane. The positive terminal goes directly to a SOT23 P-channel power MOSFET and then to the _On/Off_ switch. From here, the supply voltage path has a few vias: one to the power LED, one probably supplying an IC with power, and another going to an unpopulated 3-pin connector.

At this point, a few things should be noted. The solder work is not great. A few connectors have visibly cold joints where not enough flux was provided and/or the iron was removed before the joint fully bonded. It looks like someone was eating a glazed donut while making this board, although I know that is just reflow artifacts. Drill holes are not clean: the little sliver of metal around the board edge was not cut away. There are two plated holes for the volume knob mechanical strain relief, but these are not soldered. The pins are bent out a bit to make a spring contact. A lot of markings for SMD capacitors are not populated. I think this still counts as a few femtofarads. To nitpick: one of the capacitor markings has a lowercase _c_ while the others are uppercase. There are a lot of unpopulated-but-marked sections of the board, so this is probably a generic design that gets different components for each grade of instrument. (This is a good thing, since we might need to add filter elements or diodes or swap out existing parts to get a clean sound.) All vias go all the way through; nice for measurement probes as well as for creating a simulation.

ICs: One **TDA2822M** (8-pin) _is intended for use as dual audio power amplifier in portable cassette players_. The larger 14-pin housing is a **TL064C** quad-channel operational amplifier (_op amp_).

It looks like only two of the op amp channels are used. From a production standpoint, this makes perfect sense. Either the part is cheaper than a dual-channel IC, or maybe they have a ton of designs where a few use two channels and others use four so buying a single part in bulk is generally easiest? (Maybe the routing for other channels is hidden beneath the IC and uses a via to the other side, hidden by the EQ fader?)


## Simulation

<mark>TODO</mark>

[Falstad](https://www.falstad.com/circuit/circuitjs.html) is my tool of choice for efficient circuit simulation.

<mark>in progress</mark>

<pre>
$ 3 0.000015625 1.1208435524800693 50 0.25 43 5e-11
R 408 472 408 504 0 0 40 8.7 0 0 0.5
s 568 -112 568 -176 0 0 false
162 152 -96 152 -128 2 default-led 1 0 0 0.01
368 -56 -144 -88 -144 1 0 B
209 456 -128 504 -128 0 0.00009999999999999999 0.0010001842676502692 0.001 1
209 456 -96 504 -96 0 0.00009999999999999999 4.271584048112268 0.001 1
209 456 -64 504 -64 0 0.00009999999999999999 8.201935654612322 0.001 1
209 472 -16 520 -16 0 0.00009999999999999999 0.000999998156629108 0.001 1
209 472 16 520 16 0 0.00009999999999999999 0.001 0.001 1
209 472 48 520 48 0 0.00009999999999999999 0.0009999999998153398 0.001 1
209 472 80 520 80 0 0.00009999999999999999 0.001 0.001 1
v 312 -216 312 -264 0 1 440 0.2 0 0 0.5
c 248 -264 312 -264 0 2e-9 0.2129362809846285 0.001
r 184 -264 248 -264 0 180
w 184 -216 312 -216 0
r 16 -144 72 -144 0 10000
r 72 -144 120 -144 0 10000
r 72 -112 120 -112 0 10000
c 56 -112 16 -112 0 1.0000000000000002e-14 0.001000184267650278 0.001
c 72 -176 16 -176 0 1.0000000000000002e-14 4.271584048126987 0.001
f 520 424 472 424 33 1.5 0.02
w 408 472 408 440 0
w 408 440 472 440 0
g 520 424 520 456 0 0
w 472 408 472 392 0
w 472 392 568 392 0
w 568 392 568 -112 0
w 568 -176 248 -176 0
w 120 -176 120 -144 0
w 120 -176 120 -192 0
w 104 -208 8 -208 0
w 8 -208 -8 -192 0
w -8 -192 -8 -160 0
w -8 -160 -24 -144 0
w -24 -144 -56 -144 0
w 120 -144 120 -112 0
w 72 -176 72 -144 0
w 16 -176 16 -144 0
g 16 -112 16 -96 0 0
w 16 -112 16 -144 0
g 504 -128 520 -128 0 0
g 504 -96 520 -96 0 0
g 520 80 536 80 0 0
g 152 -128 152 -136 0 0
w 72 -112 72 -96 0
w 80 -88 144 -88 0
w 72 -136 64 -128 0
w 64 -128 64 -80 0
w 72 -96 80 -88 0
w 144 -88 152 -96 0
w 72 -136 72 -144 0
w 72 -176 80 -184 0
w 96 -184 112 -168 0
w 112 -168 392 -168 0
w 392 -168 408 -152 0
w 408 -152 408 -104 0
w 408 -104 416 -96 0
w 416 -96 456 -96 0
r 88 -184 96 -184 0 0.000001
w 56 -112 56 -96 0
w 56 -96 72 -80 0
w 72 -80 160 -80 0
w 160 -80 200 -128 0
w 216 -128 456 -128 0
r 208 -128 216 -128 0 0.000001
w 200 -128 208 -128 0
w 80 -184 88 -184 0
w 120 -192 104 -208 0
g 248 0 248 8 0 0
w 248 -176 120 -176 0
w 248 -176 248 -64 0
409 208 -32 288 -32 1 0.6 -8.580614233150278 0.0231 0
w 160 -80 160 -64 0
w 160 -64 176 -48 0
w 176 -48 208 -48 0
w 288 -32 312 -56 0
w 312 -64 456 -64 0
w 504 -72 504 -64 0
w 384 -72 80 -72 0
r 24 -32 48 -32 0 3.9
r 56 -32 80 -32 0 10000
c 24 0 48 0 0 1.0000000000000002e-10 -0.1816799729919663 0.001
r 24 32 48 32 0 47000
c 56 0 80 0 0 1.0000000000000002e-14 0.000999998156629108 0.001
c 16 -32 -8 -32 0 1.0000000000000002e-14 8.201935654620327 0.001
w 80 0 104 24 0
w 112 24 360 24 0
w 360 24 408 -32 0
w 408 -32 520 -32 0
w 520 -32 520 -16 0
r 104 24 112 24 0 0.000001
w 472 -16 400 -16 0
w 400 -16 360 32 0
w 360 32 96 32 0
w 96 32 80 16 0
w 80 16 56 16 0
w 56 16 56 0 0
w 56 16 56 24 0
w 56 24 48 32 0
174 0 72 40 48 1 15000 0.5594 Volume
w 24 48 24 32 0
g 0 72 0 88 0 0
g 184 -216 184 -200 0 0
w 184 -264 168 -248 0
w 168 -248 168 -216 0
w 168 -216 160 -208 0
w 160 -208 144 -208 0
w 144 -208 128 -192 0
w 128 -192 128 72 0
w 128 72 112 88 0
w 112 88 32 88 0
w 32 88 24 96 0
c 24 96 24 120 0 1.0000000000000002e-10 -4.237725622467233 0.001
r 24 128 0 128 0 1000000
w 24 120 24 128 0
c 24 160 0 160 0 2e-8 0.001000000380630972 0.001
w 64 -80 56 -72 0
w 56 -72 -24 -72 0
w -24 -72 -32 -64 0
w -32 -64 -32 120 0
w -32 120 -24 128 0
w -24 128 0 128 0
w 24 128 24 160 0
w 0 160 0 176 0
r 0 176 0 192 0 100
w 0 160 -24 160 0
w -24 160 -24 248 0
w -24 248 0 248 0
w 0 248 0 240 0
r 0 240 0 224 0 100
O 0 224 32 224 0 0
O 0 192 32 192 0 0
b -16 184 66 231 0
x -1 212 28 215 4 16 MIC
x -1 284 55 287 4 16 PHONE
b -16 256 66 303 0
O 0 264 32 264 0 0
O 0 296 32 296 0 0
x -1 356 34 359 4 16 LINE
b -16 328 90 375 0
O 0 336 32 336 0 0
O 0 368 32 368 0 0
w 0 264 -48 264 0
w 0 296 -40 296 0
w -40 296 -48 288 0
w -48 288 -48 264 0
w 24 0 -8 0 0
w -8 0 -8 -32 0
w -8 0 -40 0 0
w -40 0 -48 8 0
w -48 8 -48 264 0
w 312 -56 312 -64 0
w 312 -64 88 -64 0
w 88 -64 80 -56 0
w 80 -56 24 -56 0
w 24 -56 16 -48 0
w 16 -48 16 -32 0
w 80 -72 72 -64 0
w 72 -64 0 -64 0
w 0 -64 -8 -56 0
w -8 -56 -8 -32 0
r 384 -72 392 -72 0 0.000001
w 392 -72 504 -72 0
w 0 264 16 248 0
g 88 288 88 304 0 0
r 88 288 88 264 0 4
c 192 64 216 64 0 1.0000000000000002e-14 -0.0009999999998153398 0.001
r 232 64 256 64 0 100000
r 192 96 216 96 0 10000
r 232 96 256 96 0 10000
w 216 64 224 64 0
w 224 64 232 64 0
w 216 96 232 96 0
w 224 64 224 56 0
w 224 56 464 56 0
w 464 56 472 48 0
w 520 48 520 40 0
w 520 40 464 40 0
r 464 40 456 40 0 0.000001
w 456 40 448 48 0
w 448 48 192 48 0
w 192 48 192 64 0
w 192 64 80 64 0
w 80 64 72 72 0
w 72 72 48 72 0
c 216 120 216 144 0 1.0000000000000002e-14 0.001 0.001
w 216 120 216 96 0
w 216 120 240 120 0
w 80 -32 144 -32 0
w 144 -32 160 -16 0
w 160 -16 208 -16 0
w 56 -32 48 -32 0
w 48 0 48 -8 0
w 48 -8 32 -24 0
w 32 -24 24 -24 0
w 24 -24 24 -32 0
w 16 248 72 248 0
w 72 248 88 264 0
c 432 112 408 112 0 0.00001 0 0.001
r 432 112 456 112 0 1000
r 432 136 456 136 0 1000
409 224 216 224 296 1 0.6 0 0.0231 0
409 312 296 312 216 1 0.6 0 0.0231 0
c 168 144 144 144 0 0.00001 0 0.001
r 144 168 168 168 0 1000
r 144 192 168 192 0 1000
r 400 168 424 168 0 1000
c 400 200 424 200 0 0.00001 0 0.001
r 432 200 456 200 0 1000
r 416 232 440 232 0 1000
r 264 168 288 168 0 1000
r 240 336 264 336 0 1000
r 144 248 144 272 0 1000
r 144 304 168 304 0 1000
c 168 344 168 368 0 0.00001 0 0.001
r 192 368 192 392 0 1000
w 168 368 168 392 0
w 192 368 192 344 0
r 208 408 232 408 0 1000
w 208 408 192 408 0
w 192 408 192 392 0
w 192 408 192 424 0
o 11 1 0 12291 0.19999845787628553 0.0001 0 2 11 3
o 165 1 0 12291 0.0001 0.0001 1 2 165 3
</pre>

- https://datasheetspdf.com/datasheet/A1SHB.html
- https://www.st.com/resource/en/datasheet/tda2822m.pdf
- https://www.st.com/resource/en/datasheet/tl064.pdf
- https://www.digikey.de/en/resources/conversion-calculators/conversion-calculator-smd-resistor-code


## Outcome

<mark>TODO</mark>

- Background noise?
    - TODO
- Static while adjusting settings?
    - TODO
- EQ faders? 
    - TODO
- Each output has sound balanced between both channels?
    - TODO
- Purpose of the auxiliary port?
    - TODO
- Overdriving the amplifier?
    - TODO

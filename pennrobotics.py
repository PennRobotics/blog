from pygments.style import Style
from pygments.token import Keyword, Name, Comment, String, Error, Text, \
     Number, Operator, Generic, Whitespace, Punctuation, Other, Literal

REDDISH = '#f43753'
BLUEISH = '#537adf'
ACCENTS = '#d3b987'

class PennRoboticsStyle(Style):
    """
    This style was modified from https://blog.thea.codes and is near the one I
    use for Vim, which was originally a derivative of zellner in dark mode.
    """

    background_color = '#282828'
    highlight_color = '#335261'

    styles = {
        Text:                      '#ffffff',  # class ''
        Whitespace:                '#ffc24b',  # class 'w'
        Error:          '#ff6252 bg:#1d1d1d',  # class 'err'
        Other:                     '',         # class 'x'

        Comment:                   '#d3b987',  # class 'c'
        Comment.Multiline:         '',         # class 'cm'
        Comment.Preproc:           '',         # class 'cp'
        Comment.Single:            '',         # class 'c1'
        Comment.Special:           '',         # class 'cs'

        Keyword:                   '#c9d05c',  # class 'k'  italic?
        Keyword.Constant:          '',         # class 'kc'
        Keyword.Declaration:       '',         # class 'kd' italic?
        Keyword.Namespace:         '#7ecef4',  # class 'kn'
        Keyword.Pseudo:            '',         # class 'kp'
        Keyword.Reserved:          '',         # class 'kr'
        Keyword.Type:              '',         # class 'kt' italic?

        Operator:                  '#7ecef4',  # class 'o'
        Operator.Word:             '',         # class 'ow' - like keywords

        Punctuation:               '#ffffff',  # class 'p'

        Name:                      '#ffffff',  # class 'n'
        Name.Attribute:            REDDISH,    # class 'na'
        Name.Builtin:              '',         # class 'nb'
        Name.Builtin.Pseudo:       '#73cef4',  # class 'bp'
        Name.Class:                REDDISH,    # class 'nc' italic underline?
        Name.Constant:             '#b3deef',  # class 'no'
        Name.Decorator:            REDDISH,    # class 'nd' underline?
        Name.Entity:               '',         # class 'ni'
        Name.Exception:            REDDISH,    # class 'ne' underline?
        Name.Function:             REDDISH,    # class 'nf'
        Name.Property:             '#ffffff',  # class 'py'
        Name.Label:                '',         # class 'nl'
        Name.Namespace:            '',         # class 'nn' (TODO)
        Name.Other:                '',         # class 'nx'
        Name.Tag:                  '#7ecef4',  # class 'nt' - like a keyword
        Name.Variable:             '#ffffff',  # class 'nv' (TODO)
        Name.Variable.Class:       '',         # class 'vc' (TODO)
        Name.Variable.Global:      '',         # class 'vg' (TODO)
        Name.Variable.Instance:    '',         # class 'vi' (TODO)

        Number:                    '#d3b987',  # class 'm'
        Number.Float:              '',         # class 'mf'
        Number.Hex:                '',         # class 'mh'
        Number.Integer:            '',         # class 'mi'
        Number.Integer.Long:       '',         # class 'il'
        Number.Oct:                '',         # class 'mo'

        Literal:                   '#b3deef',  # class 'l'
        Literal.Date:              '#ffc24b',  # class 'ld'

        String:                    BLUEISH,    # class 's'
        String.Backtick:           '',         # class 'sb'
        String.Char:               '',         # class 'sc'
        String.Doc:                '',         # class 'sd' - like a comment
        String.Double:             '',         # class 's2'
        String.Escape:             '',         # class 'se'
        String.Heredoc:            '',         # class 'sh'
        String.Interpol:           '',         # class 'si'
        String.Other:              '',         # class 'sx'
        String.Regex:              '',         # class 'sr'
        String.Single:             '',         # class 's1'
        String.Symbol:             '',         # class 'ss'

        Generic:                   '',         # class 'g'
        Generic.Deleted:           '#f43753',  # class 'gd'
        Generic.Emph:              'italic',   # class 'ge'
        Generic.Error:             '',         # class 'gr'
        Generic.Heading:           '',         # class 'gh'
        Generic.Inserted:          '#c9d05c',  # class 'gi'
        Generic.Output:            '',         # class 'go'
        Generic.Prompt:            '',         # class 'gp'
        Generic.Strong:            'bold',     # class 'gs'
        Generic.Subheading:        '#ffc24b',  # class 'gu'
        Generic.Traceback:         '',         # class 'gt'
    }
